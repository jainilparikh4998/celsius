package sheridan;

public class Celsius {

	public static void main(String[] args) {
		System.out.println("C: " + Celsius.convertFromFahrenheit(32));

	}
	
	public static int convertFromFahrenheit(int value) {
		int celsius =(( 5 * (value - 32)) / 9);  
		return Math.round(celsius);
	}
}
