package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CelsiusTest {

	@Test
	public void testconvertFromFahrenheit() {
		int celsius = Celsius.convertFromFahrenheit(31);
		assertTrue("t", celsius == 0);
	}
	
	@Test
	public void testconvertFromFahrenheitNegative() {
		int celsius = Celsius.convertFromFahrenheit(31);
		assertFalse("t" , celsius == -1);
	}
	
	@Test
	public void testconvertFromFahrenheitBoundryIn() {
		int celsius = Celsius.convertFromFahrenheit(6);
		assertTrue("t", celsius == -14);
	}
	
	@Test
	public void testconvertFromFahrenheitBoundryOut() {
		int celsius = Celsius.convertFromFahrenheit(7);
		assertFalse("t" , celsius == -14);
	}
}

